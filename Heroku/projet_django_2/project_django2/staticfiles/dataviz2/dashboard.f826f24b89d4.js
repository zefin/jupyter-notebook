function show_chart(label, donnees) {
    var ctx = document.getElementById('myChart')
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: label,
        datasets: [{
          data: donnees,
          lineTension: 0,
          backgroundColor: 'grey',
          borderColor: 'green',
          borderWidth: 4,
          pointBackgroundColor: '#007bff'
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        },
        legend: {
          display: false
        }
      }
    })
  }
  