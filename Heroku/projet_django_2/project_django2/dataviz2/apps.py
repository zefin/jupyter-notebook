from django.apps import AppConfig


class Dataviz2Config(AppConfig):
    name = 'dataviz2'
