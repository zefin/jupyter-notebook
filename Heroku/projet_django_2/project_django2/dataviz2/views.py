from django.shortcuts import render
from .models import Letter
import io
from django.http import FileResponse
from reportlab.pdfgen import canvas

import csv
from django.shortcuts import render
from .models import Election
from django.http import HttpResponse

def home(request):
    return render(request, 'dataviz2/home.html')

def import_csv(request):
    if request.method == "GET":
        return render(request, 'dataviz2/import_csv.html')
    file_csv = request.FILES['file']
    data = [row for row in csv.reader(file_csv.read().decode('UTF-8').splitlines())]
    for row in data[1:]:
        Election.objects.create(
                bureau=row[0],
                votants=row[1],
                )
    return render(request, 'dataviz2/home.html')

def visu(request):
    context = {
        'label' : [elt.bureau for elt in Election.objects.all()],
        'data' : [elt.votants for elt in Election.objects.all()],
    }
    return render(request, 'dataviz2/visu.html', context=context)


def export_pdf(request):
    # Create a file-like buffer to receive PDF data.
    buffer = io.BytesIO()

    # Create the PDF object, using the buffer as its "file."
    p = canvas.Canvas(buffer)

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    p.drawString(100, 100, "Hello world.")

    # Close the PDF object cleanly, and we're done.
    p.showPage()
    p.save()

    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    buffer.seek(0)
    return FileResponse(buffer, as_attachment=True, filename='hello.pdf')

def export_csv(request):
    #Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'

    writer = csv.writer(response)
    writer.writerow(['First row', 'Foo', 'Bar', 'Baz'])
    writer.writerow(['Second row', 'A', 'B', 'C', '"Testing"', "Here's a quote"])

    return response

#def export_csv(request):
     #all_entries = Election.objects.all()
     #return all_entries