from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('V1', views.import_csv, name='import_csv'),
    path('V2', views.visu, name='visu'),
    path('V3', views.export_pdf, name='export_pdf'),
    #path('V4', views.export_csv, name='export_csv'),
    path('V5', views.export_csv, name='export_csv'),
]
