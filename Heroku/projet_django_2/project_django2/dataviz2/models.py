from django.db import models

class Letter(models.Model):
    label = models.CharField(max_length=5)
    data = models.IntegerField()

    def __str__(self):
        return f"{self.label} - {self.data}"

class Election(models.Model):
    bureau = models.CharField(max_length=200)
    votants = models.IntegerField()

    def __str__(self):
        return f"Données du bureau {self.bureau} - Votants {str(self.votants)}"

